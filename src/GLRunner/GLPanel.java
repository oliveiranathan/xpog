/*
 * Copyright (C) 2014, Nathan Oliveira <oliveiranathan(at)gmail(dot)com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package GLRunner;

import Geom.Instrument;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author Nathan Oliveira
 */
public class GLPanel implements Runnable {

	private final DisplayMode mode;
	private final int width, height;
	private final boolean isSizedAsAsked;
	private int xIni, yIni;
	private boolean run;
	private final Instrument instrument;

	public GLPanel(Instrument instrument) {
		if (Display.isCreated()) {
			Display.destroy();
		}
		this.width = instrument.getW();
		this.height = instrument.getH();
		if (width > Display.getDesktopDisplayMode().getWidth() || height > Display.getDesktopDisplayMode().getHeight()) {
			mode = Display.getDesktopDisplayMode();
			isSizedAsAsked = false;
		} else {
			mode = new DisplayMode(width, height);
			isSizedAsAsked = true;
		}
		run = true;
		this.instrument = instrument;
		instrument.setSliders();
		Sliders.createFrame();
	}

	@Override
	public void run() {
		try {
			Display.setDisplayMode(mode);
			Display.create();
			Display.sync(60);
			GL11.glMatrixMode(GL11.GL_PROJECTION);
			GL11.glLoadIdentity();
			GL11.glOrtho(0, mode.getWidth(), 0, mode.getHeight(), -1, 1);
			GL11.glClearColor(1, 1, 1, 1);
//			GL11.glPolygonMode(GL11.GL_FRONT_AND_BACK, GL11.GL_LINE);
			while (run && !Display.isCloseRequested()) {
				GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
				instrument.drawGL();
				processInput();
				Display.update();
				Display.sync(60);
			}
			Display.destroy();
		} catch (LWJGLException ex) {
			Logger.getLogger(GLPanel.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private void processInput() {
		if (!isSizedAsAsked) {
			xIni += Mouse.getDX();
			yIni += Mouse.getDY();
		} // TODO implement the option to drag the image if the panel is not exactly the size of the window...
	}

	public void stop() {
		run = false;
	}

	public void start() {
		run = true;
		new Thread(this).start();
	}

	@SuppressWarnings("PublicInnerClass")
	public static class Sliders {

		private static final HashMap<String, Float> values = new HashMap<>(5);
		private static final HashMap<String, SliderPanel> sliders = new HashMap<>(5);

		public static float getValue(String name) {
			return values.getOrDefault(name, 0.f);
		}

		public static void putValue(String name, float value) {
			values.put(name, value);
		}

		private static void createFrame() {
			if (values.isEmpty()) {
				return;
			}
			JFrame frame = new JFrame();
			frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
			frame.addWindowListener(new WindowAdapter() {

				@Override
				public void windowClosing(WindowEvent we) {
					sliders.clear();
					frame.dispose();
				}
			});
			JScrollPane pane = new JScrollPane();
			JPanel panel = new JPanel();
			panel.setLayout(new BoxLayout(panel, BoxLayout.LINE_AXIS));
			values.keySet().stream().forEach((value) -> {
				SliderPanel sp = new SliderPanel();
				sp.setDRefName(value);
				sliders.put(value, sp);
				panel.add(sp);
			});
			pane.add(panel);
			frame.add(pane);
			frame.pack();
			frame.setVisible(true);
		}

		public static void notifyChange() {
			sliders.keySet().stream().forEach((value) -> values.put(value, 0.f + sliders.get(value).getValue()));
		}

		private Sliders() {
		}
	}
}
