/*
 * Copyright (C) 2014, Nathan Oliveira <oliveiranathan(at)gmail(dot)com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package Geom;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Nathan Oliveira
 */
public class XPanel implements Serializable {

	private static final long serialVersionUID = 1L;

	private static List<XPanelObserver> observers = new ArrayList<>(1);

	private static XPanel instance = null;

	public static XPanel getInstance() throws IllegalStateException {
		if (instance == null) {
			throw new IllegalStateException("Create a instance first.");
		}
		return instance;
	}

	public static void createInstance() {
		instance = new XPanel();
	}

	public static void createInstance(File file) throws IOException, FileNotFoundException, ClassNotFoundException {
		instance = new XPanel(file);
	}

	protected static void notifyObservers() {
		observers.stream().forEach((obs) -> obs.notifyObservers());
	}

	public static void addObserver(XPanelObserver observer) {
		observers.add(observer);
	}

	private final List<Instrument> instruments;
	private File file;

	private XPanel() {
		this.instruments = new ArrayList<>(1);
		file = null;
		observers = new ArrayList<>(1);
	}

	private XPanel(File file) throws IOException, FileNotFoundException, ClassNotFoundException {
		this.file = file;
		instruments = processFile();
		observers = new ArrayList<>(1);
	}

	@SuppressWarnings("unchecked")
	private List<Instrument> processFile() throws FileNotFoundException, IOException, ClassNotFoundException {
		List<Instrument> list;
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
			list = (List<Instrument>) ois.readObject();
		}
		return list;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public void saveFile() throws IOException {
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
			oos.writeObject(instruments);
		}
	}

	public void export(File f) throws IOException, ParseException, NumberFormatException, IndexOutOfBoundsException, IllegalStateException {
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(f))) {
			writer.write("/*\n"
					+ " * This file was created by the X-Plane plugin generator.\n"
					+ " * It contains methods that can draw every instrument,\n"
					+ " * layer and element made in the design.\n"
					+ " * \n"
					+ " * It will still need to be compiled into a dll for X-Plane.\n"
					+ " */\n\n");
			writer.write("#include \"XPLMDataAccess.h\"\n\n");
			writer.write("namespace opengl_methods {\n\n");
			// get the X-Plane pointers:
			List<String> listDRefs = new ArrayList<>(10);
			for (Instrument instrument : instruments) {
				for (Layer layer : instrument.getLayers()) {
					listDRefs.addAll(layer.getXPlaneRefPointers());
				}
			}
			HashMap<String, String> dRefMap = new HashMap<>(listDRefs.size());
			listDRefs.stream().forEach((dRef) -> dRefMap.put(dRef, dRef.replaceAll("\\W", "_")));
			listDRefs.clear();
			dRefMap.keySet().stream().sorted().forEach((dRef) -> listDRefs.add(dRef));
			// find the DataRefs
			for (String dRef : listDRefs) {
				writer.write("\tXPLMDataRef " + dRefMap.get(dRef) + " = XPLMFindDataRef(\"" + dRef + "\");\n");
			}
			if (!listDRefs.isEmpty()) {
				writer.write("\n");
			}
			// create the "globals" with the data
			for (String dRef : listDRefs) {
				writer.write("\tfloat f" + dRefMap.get(dRef) + " = 0.f;\n");
			}
			if (!listDRefs.isEmpty()) {
				writer.write("\n");
			}
			// create the methods:
			writer.write("\tvoid draw_all() {\n");
			writer.write("\t\tglMatrixMode(GL_MODELVIEW);\n");
			writer.write("\t\tglPushMatrix();\n");
			writer.write("\t\tglLoadIdentity();\n");
			for (String dRef : listDRefs) { // Update the "global" data
				writer.write("\t\tf" + dRefMap.get(dRef) + " = XPLMGetDataf(" + dRefMap.get(dRef) + ");\n");
			}
			for (Instrument instrument : instruments) { // call the instruments methods
				writer.write("\t\tdraw_instrument_" + instrument.getName().replaceAll("\\W", "_") + "();\n");
			}
			writer.write("\t\tglPopMatrix();\n");
			writer.write("\t} // end draw_all()\n\n");
			int i = 1;
			for (Instrument instrument : instruments) { // create the instruments methods
				instrument.export(writer, i);
				i++;
			}
			writer.write("}// end namespace\n");
			writer.flush();
		}
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Instrument> getInstruments() {
		return instruments;
	}

	public File getFile() {
		return file;
	}

	public void addInstrument(String name) throws IllegalArgumentException {
		for (Instrument i : instruments) {
			if (i.getName().equals(name)) {
				throw new IllegalArgumentException("The name already exists.");
			}
		}
		instruments.add(new Instrument(name));
		XPanel.notifyObservers();
	}

	public void removeInstrument(Instrument instrument) {
		instruments.remove(instrument);
		XPanel.notifyObservers();
	}

	public void swapInstruments(int a, int b) {
		if (a == b || a < 0 || b < 0 || a >= instruments.size() || b >= instruments.size()) {
			return;
		}
		Instrument i = instruments.get(a);
		instruments.set(a, instruments.get(b));
		instruments.set(b, i);
		notifyObservers();
	}
}
