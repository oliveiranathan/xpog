/*
 * Copyright (C) 2014, Nathan Oliveira <oliveiranathan(at)gmail(dot)com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package Geom;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.io.BufferedWriter;
import java.io.IOException;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author Nathan Oliveira
 */
public class Arc extends Element {

	private static final long serialVersionUID = 1L;

	private Point center;
	private int radius;
	private int angleStart;
	private int angleSweep;

	public Arc(String name, Color color, Point center, int radius, int angleStart, int angleSweep) {
		super(name, color);
		this.center = center;
		this.radius = radius;
		this.angleStart = angleStart;
		this.angleSweep = angleSweep;
	}

	@Override
	public void draw(Graphics g) {
		super.draw(g);
		if (isFilled()) {
			g.fillArc(center.x - radius, center.y - radius, 2 * radius, 2 * radius, angleStart, angleSweep);
		} else {
			g.drawArc(center.x - radius, center.y - radius, 2 * radius, 2 * radius, angleStart, angleSweep);
		}
	}

	@Override
	public void drawBorder(Graphics g) {
		super.drawBorder(g);
		g.drawArc(center.x - radius, center.y - radius, 2 * radius, 2 * radius, angleStart, angleSweep);
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public void setCenter(Point center) {
		this.center = center;
	}

	public void setAngleSweep(int angleSweep) {
		this.angleSweep = angleSweep;
	}

	public void setAngleStart(int angleStart) {
		this.angleStart = angleStart;
	}

	public int getRadius() {
		return radius;
	}

	public Point getCenter() {
		return center;
	}

	public int getAngleSweep() {
		return angleSweep;
	}

	public int getAngleStart() {
		return angleStart;
	}

	@Override
	public void export(BufferedWriter writer) throws IOException {
		super.export(writer);
		Layer parent = getParent();
		if (isFilled()) {
			writer.write("\t\tglBegin(GL_POLYGON);\n");
			writer.write("\t\t\tglVertex2f(" + (center.x - parent.getBasePoint().x) + "," + (center.y - parent.getBasePoint().y) + ");\n");
		} else {
			writer.write("\t\tglBegin(GL_LINE_STRIP);\n");
		}
		double nEdges = Math.toRadians(angleSweep) * radius / 10.0;
		for (double i = Math.toRadians(angleStart); i <= Math.toRadians(angleSweep); i += Math.toRadians(angleSweep) / nEdges) {
			writer.write("\t\t\tglVertex2f(" + (center.x - parent.getBasePoint().x + Math.cos(i) * radius) + "," + (center.y - parent.getBasePoint().y + Math.sin(i) * radius) + ");\n");
		}
		writer.write("\t\tglEnd();\n");
	}

	@Override
	public void drawGL() {
		super.drawGL();
		Layer parent = getParent();
		if (isFilled()) {
			GL11.glBegin(GL11.GL_POLYGON);
			GL11.glVertex2f(center.x - parent.getBasePoint().x, center.y - parent.getBasePoint().y);
		} else {
			GL11.glBegin(GL11.GL_LINE_STRIP);
		}
		double nEdges = Math.toRadians(angleSweep) * radius / 10.0;
		for (double i = Math.toRadians(angleStart); i <= Math.toRadians(angleSweep); i += Math.toRadians(angleSweep) / nEdges) { // TODO fix this
			GL11.glVertex2f((float) (center.x - parent.getBasePoint().x + Math.cos(i) * radius), (float) (center.y - parent.getBasePoint().y + Math.sin(i) * radius));
		}
		GL11.glEnd();
	}
}
