/*
 * Copyright (C) 2014, Nathan Oliveira <oliveiranathan(at)gmail(dot)com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package Geom;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.io.BufferedWriter;
import java.io.IOException;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author Nathan Oliveira
 */
public class Triangle extends Element {

	private static final long serialVersionUID = 1L;

	private Point p1, p2, p3;

	public Triangle(String name, Color color, Point p1, Point p2, Point p3) {
		super(name, color);
		this.p1 = p1;
		this.p2 = p2;
		this.p3 = p3;
	}

	@Override
	public void draw(Graphics g) {
		super.draw(g);
		if (isFilled()) {
			int[] xs = new int[3], ys = new int[3];
			xs[0] = p1.x;
			xs[1] = p2.x;
			xs[2] = p3.x;
			ys[0] = p1.y;
			ys[1] = p2.y;
			ys[2] = p3.y;
			g.fillPolygon(xs, ys, 3);
		} else {
			g.drawLine(p1.x, p1.y, p2.x, p2.y);
			g.drawLine(p2.x, p2.y, p3.x, p3.y);
			g.drawLine(p3.x, p3.y, p1.x, p1.y);
		}
	}

	@Override
	public void drawBorder(Graphics g) {
		super.drawBorder(g);
		g.drawLine(p1.x, p1.y, p2.x, p2.y);
		g.drawLine(p2.x, p2.y, p3.x, p3.y);
		g.drawLine(p3.x, p3.y, p1.x, p1.y);
	}

	public Point getP1() {
		return p1;
	}

	public void setP1(Point p1) {
		this.p1 = p1;
	}

	public Point getP2() {
		return p2;
	}

	public void setP2(Point p2) {
		this.p2 = p2;
	}

	public Point getP3() {
		return p3;
	}

	public void setP3(Point p3) {
		this.p3 = p3;
	}

	@Override
	public void export(BufferedWriter writer) throws IOException {
		super.export(writer);
		Layer parent = getParent();
		if (isFilled()) {
			writer.write("\t\tglBegin(GL_POLYGON);\n");
		} else {
			writer.write("\t\tglBegin(GL_LINE_LOOP);\n");
		}
		writer.write("\t\t\tglVertex2f(" + (p1.x - parent.getBasePoint().x) + "," + (p1.y - parent.getBasePoint().y) + ");\n");
		writer.write("\t\t\tglVertex2f(" + (p2.x - parent.getBasePoint().x) + "," + (p2.y - parent.getBasePoint().y) + ");\n");
		writer.write("\t\t\tglVertex2f(" + (p3.x - parent.getBasePoint().x) + "," + (p3.y - parent.getBasePoint().y) + ");\n");
		writer.write("\t\tglEnd();\n");
	}

	@Override
	public void drawGL() {
		super.drawGL();
		Layer parent = getParent();
		if (isFilled()) {
			GL11.glBegin(GL11.GL_POLYGON);
		} else {
			GL11.glBegin(GL11.GL_LINE_LOOP);
		}
		GL11.glVertex2f(p1.x - parent.getBasePoint().x, p1.y - parent.getBasePoint().y);
		GL11.glVertex2f(p2.x - parent.getBasePoint().x, p2.y - parent.getBasePoint().y);
		GL11.glVertex2f(p3.x - parent.getBasePoint().x, p3.y - parent.getBasePoint().y);
		GL11.glEnd();
	}
}
