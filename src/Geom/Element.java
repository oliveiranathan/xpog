/*
 * Copyright (C) 2014, Nathan Oliveira <oliveiranathan(at)gmail(dot)com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package Geom;

import java.awt.Color;
import java.awt.Graphics;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Serializable;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author Nathan Oliveira
 */
public abstract class Element implements Serializable {

	private static final long serialVersionUID = 1L;

	private Color color;
	private String name;
	private boolean filled = true;

	public Element(String name, Color color) {
		this.color = color;
		this.name = name;
	}

	public void draw(Graphics g) {
		g.setColor(color);
	}

	public void drawBorder(Graphics g) {
		g.setColor(Color.MAGENTA);
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
		XPanel.notifyObservers();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		XPanel.notifyObservers();
	}

	public boolean isFilled() {
		return filled;
	}

	public void setFilled(boolean filled) {
		this.filled = filled;
	}

	public Layer getParent() {
		for (Instrument instrument : XPanel.getInstance().getInstruments()) {
			for (Layer layer : instrument.getLayers()) {
				if (layer.getElements().contains(this)) {
					return layer;
				}
			}
		}
		return null;
	}

	public void export(BufferedWriter writer) throws IOException {
		writer.write("\t\tglColor3ub(" + color.getRed() + ", " + color.getGreen() + ", " + color.getBlue() + ");\n");
	}

	public void drawGL() {
		GL11.glColor3ub((byte) color.getRed(), (byte) color.getGreen(), (byte) color.getBlue());
	}
}
