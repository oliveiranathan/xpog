/*
 * Copyright (C) 2014, Nathan Oliveira <oliveiranathan(at)gmail(dot)com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package Geom;

import GLRunner.GLPanel;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 *
 * @author Nathan Oliveira
 */
public class Instrument implements Serializable {

	private static final long serialVersionUID = 1L;

	private final List<Layer> layers;
	private String name;
	private transient BufferedImage background;
	private int x, y; // The start position of the instrument in the panel, in pixels.
	private int w, h; // The size of the instrument in the panel, in pixels.

	public Instrument(String name) {
		this.layers = new ArrayList<>(1);
		this.name = name;
		background = null;
		x = 0;
		y = 0;
		w = 0;
		h = 0;
	}

	public int getW() {
		return w;
	}

	public void setW(int w) {
		this.w = w;
	}

	public int getH() {
		return h;
	}

	public void setH(int h) {
		this.h = h;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public BufferedImage getBackground() {
		return background;
	}

	public void setBackground(BufferedImage background) {
		this.background = background;
		XPanel.notifyObservers();
	}

	public void setName(String name) {
		this.name = name;
		XPanel.notifyObservers();
	}

	public String getName() {
		return name;
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Layer> getLayers() {
		return layers;
	}

	public void addLayer(String name) throws IllegalArgumentException {
		for (Layer l : layers) {
			if (l.getName().equals(name)) {
				throw new IllegalArgumentException("The name already exists.");
			}
		}
		layers.add(new Layer(name));
		XPanel.notifyObservers();
	}

	private void writeObject(ObjectOutputStream oos) throws IOException {
		oos.defaultWriteObject();
		if (background == null) {
			oos.writeBoolean(false);
		} else {
			oos.writeBoolean(true);
			ImageIO.write(background, "png", oos);
		}
	}

	private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
		ois.defaultReadObject();
		if (ois.readBoolean()) {
			background = ImageIO.read(ois);
		}
	}

	public void swapLayers(int a, int b) {
		if (a == b || a < 0 || b < 0 || a >= layers.size() || b >= layers.size()) {
			return;
		}
		Layer i = layers.get(a);
		layers.set(a, layers.get(b));
		layers.set(b, i);
		XPanel.notifyObservers();
	}

	public void removeLayer(Layer layer) {
		layers.remove(layer);
		XPanel.notifyObservers();
	}

	public void export(BufferedWriter writer, int i) throws IOException, NumberFormatException, IndexOutOfBoundsException, IllegalStateException {
		writer.write("\tvoid draw_instrument_" + name.replaceAll("\\W", "_") + "() {\n");
		for (Layer layer : layers) { // calls the layers methods
			writer.write("\t\tdraw_layer_" + i + layer.getName().replaceAll("\\W", "_") + "();\n");
		}
		writer.write("\t} // end draw_instrument_" + name.replaceAll("\\W", "_") + "()\n\n");
		for (Layer layer : layers) { // creates the layers methods
			layer.export(writer, i);
		}
	}

	public void drawGL() {
		layers.stream().forEach((layer) -> layer.drawGL());
	}

	public void setSliders() {
		List<String> listDRefs = new ArrayList<>(10);
		layers.stream().forEach((layer) -> {
			try {
				listDRefs.addAll(layer.getXPlaneRefPointers());
			} catch (ParseException ex) {
				Logger.getLogger(Instrument.class.getName()).log(Level.SEVERE, null, ex);
			}
		});
		HashMap<String, String> dRefMap = new HashMap<>(listDRefs.size());
		listDRefs.stream().forEach((dRef) -> dRefMap.put(dRef, dRef.replaceAll("\\W", "_")));
		listDRefs.clear();
		dRefMap.keySet().stream().sorted().forEach((dRef) -> listDRefs.add(dRef));
		listDRefs.stream().forEach((dRef) -> GLPanel.Sliders.putValue("f" + dRefMap.get(dRef), 0));
	}
}
