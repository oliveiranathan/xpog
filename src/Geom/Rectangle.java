/*
 * Copyright (C) 2014, Nathan Oliveira <oliveiranathan(at)gmail(dot)com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package Geom;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.io.BufferedWriter;
import java.io.IOException;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author Nathan Oliveira
 */
public class Rectangle extends Element {

	private static final long serialVersionUID = 1L;

	private Point pa, pb, pc, pd;

	public Rectangle(String name, Color color, Point a, Point b, Point c, Point d) {
		super(name, color);
		pa = a;
		pb = b;
		pc = c;
		pd = d;
	}

	@Override
	public void draw(Graphics g) {
		super.draw(g);
		if (isFilled()) {
			int[] xs = new int[4], ys = new int[4];
			xs[0] = pa.x;
			xs[1] = pb.x;
			xs[2] = pc.x;
			xs[3] = pd.x;
			ys[0] = pa.y;
			ys[1] = pb.y;
			ys[2] = pc.y;
			ys[3] = pd.y;
			g.fillPolygon(xs, ys, 4);
		} else {
			g.drawLine(pa.x, pa.y, pb.x, pb.y);
			g.drawLine(pb.x, pb.y, pc.x, pc.y);
			g.drawLine(pc.x, pc.y, pd.x, pd.y);
			g.drawLine(pd.x, pd.y, pa.x, pa.y);
		}
	}

	@Override
	public void drawBorder(Graphics g) {
		super.drawBorder(g);
		g.drawLine(pa.x, pa.y, pb.x, pb.y);
		g.drawLine(pb.x, pb.y, pc.x, pc.y);
		g.drawLine(pc.x, pc.y, pd.x, pd.y);
		g.drawLine(pd.x, pd.y, pa.x, pa.y);
	}

	public Point getPa() {
		return pa;
	}

	public void setPa(Point pa) {
		this.pa = pa;
	}

	public Point getPb() {
		return pb;
	}

	public void setPb(Point pb) {
		this.pb = pb;
	}

	public Point getPc() {
		return pc;
	}

	public void setPc(Point pc) {
		this.pc = pc;
	}

	public Point getPd() {
		return pd;
	}

	public void setPd(Point pd) {
		this.pd = pd;
	}

	@Override
	public void export(BufferedWriter writer) throws IOException {
		super.export(writer);
		Layer parent = getParent();
		if (isFilled()) {
			writer.write("\t\tglBegin(GL_POLYGON);\n");
		} else {
			writer.write("\t\tglBegin(GL_LINE_LOOP);\n");
		}
		writer.write("\t\t\tglVertex2f(" + (pa.x - parent.getBasePoint().x) + "," + (pa.y - parent.getBasePoint().y) + ");\n");
		writer.write("\t\t\tglVertex2f(" + (pb.x - parent.getBasePoint().x) + "," + (pb.y - parent.getBasePoint().y) + ");\n");
		writer.write("\t\t\tglVertex2f(" + (pc.x - parent.getBasePoint().x) + "," + (pc.y - parent.getBasePoint().y) + ");\n");
		writer.write("\t\t\tglVertex2f(" + (pd.x - parent.getBasePoint().x) + "," + (pd.y - parent.getBasePoint().y) + ");\n");
		writer.write("\t\tglEnd();\n");
	}

	@Override
	public void drawGL() {
		super.drawGL();
		Layer parent = getParent();
		if (isFilled()) {
			GL11.glBegin(GL11.GL_POLYGON);
		} else {
			GL11.glBegin(GL11.GL_LINE_LOOP);
		}
		GL11.glVertex2f(pa.x - parent.getBasePoint().x, pa.y - parent.getBasePoint().y);
		GL11.glVertex2f(pb.x - parent.getBasePoint().x, pb.y - parent.getBasePoint().y);
		GL11.glVertex2f(pc.x - parent.getBasePoint().x, pc.y - parent.getBasePoint().y);
		GL11.glVertex2f(pd.x - parent.getBasePoint().x, pd.y - parent.getBasePoint().y);
		GL11.glEnd();
	}
}
