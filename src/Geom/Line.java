/*
 * Copyright (C) 2014, Nathan Oliveira <oliveiranathan(at)gmail(dot)com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package Geom;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.io.BufferedWriter;
import java.io.IOException;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author Nathan Oliveira
 */
public class Line extends Element {

	private static final long serialVersionUID = 1L;

	private Point start, end;

	public Line(String name, Color color, Point start, Point end) {
		super(name, color);
		this.start = start;
		this.end = end;
	}

	public Point getStart() {
		return start;
	}

	public Point getEnd() {
		return end;
	}

	public void setStart(Point start) {
		this.start = start;
		XPanel.notifyObservers();
	}

	public void setEnd(Point end) {
		this.end = end;
		XPanel.notifyObservers();
	}

	@Override
	public void draw(Graphics g) {
		super.draw(g);
		g.drawLine(start.x, start.y, end.x, end.y);
	}

	@Override
	public void drawBorder(Graphics g) {
		super.drawBorder(g);
		g.drawLine(start.x, start.y, end.x, end.y);
	}

	@Override
	public void export(BufferedWriter writer) throws IOException {
		super.export(writer);
		Layer parent = getParent();
		writer.write("\t\tglBegin(GL_LINES);\n");
		writer.write("\t\t\tglVertex2f(" + (start.x - parent.getBasePoint().x) + "," + (start.y - parent.getBasePoint().y) + ");\n");
		writer.write("\t\t\tglVertex2f(" + (end.x - parent.getBasePoint().x) + "," + (end.y - parent.getBasePoint().y) + ");\n");
		writer.write("\t\tglEnd();\n");
	}

	@Override
	public void drawGL() {
		super.drawGL();
		Layer parent = getParent();
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex2f(start.x - parent.getBasePoint().x, start.y - parent.getBasePoint().y);
		GL11.glVertex2f(end.x - parent.getBasePoint().x, end.y - parent.getBasePoint().y);
		GL11.glEnd();
	}
}
