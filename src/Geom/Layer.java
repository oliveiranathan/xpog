/*
 * Copyright (C) 2014, Nathan Oliveira <oliveiranathan(at)gmail(dot)com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package Geom;

import GLRunner.GLPanel;
import java.awt.Point;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.lwjgl.opengl.GL11;

public class Layer implements Serializable {

	private static final long serialVersionUID = 1L;

	private final List<Element> elements;
	private String name;
	private Point basePoint;
	private int dx, dy; // distance between the base point and the start point of the instrument.
	private int scissorMinX = 0, scissorMinY = 0, scissorMaxX = 0, scissorMaxY = 0;
	private String actions;

	public Layer(String name) {
		this.elements = new ArrayList<>(5);
		this.name = name;
		actions = "";
		dx = 0;
		dy = 0;
		basePoint = new Point(0, 0);
	}

	public String getActions() {
		return actions;
	}

	public void setActions(String actions) {
		this.actions = actions;
	}

	public int getDx() {
		return dx;
	}

	public void setDx(int dx) {
		this.dx = dx;
	}

	public int getDy() {
		return dy;
	}

	public void setDy(int dy) {
		this.dy = dy;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		XPanel.notifyObservers();
	}

	@SuppressWarnings("ReturnOfCollectionOrArrayField")
	public List<Element> getElements() {
		return elements;
	}

	public void addElement(Element element) {
		elements.add(element);
		XPanel.notifyObservers();
	}

	public void setBasePoint(Point basePoint) {
		this.basePoint = basePoint;
		XPanel.notifyObservers();
	}

	public Point getBasePoint() {
		return basePoint;
	}

	public void swapElements(int a, int b) {
		if (a == b || a < 0 || b < 0 || a >= elements.size() || b >= elements.size()) {
			return;
		}
		Element i = elements.get(a);
		elements.set(a, elements.get(b));
		elements.set(b, i);
		XPanel.notifyObservers();
	}

	public void removeElement(Element element) {
		elements.remove(element);
		XPanel.notifyObservers();
	}

	public List<String> getXPlaneRefPointers() throws ParseException {
		List<String> list = new ArrayList<>(3);
		Pattern p = Pattern.compile("<([^>]*)>");// stuff between < and >: <sim/blah/blah/blah>
		Matcher m = p.matcher(actions);
		while (m.find()) {
			String s = m.group(1);
			if (s.contains("\n\t ")) {
				throw new ParseException("Error obtaining the dataref name", 0);
			}
			list.add(s);
		}
		return list;
	}

	public Instrument getParent() {
		for (Instrument instrument : XPanel.getInstance().getInstruments()) {
			if (instrument.getLayers().contains(this)) {
				return instrument;
			}
		}
		return null;
	}

	@SuppressWarnings("AssignmentToForLoopParameter")
	public void export(BufferedWriter writer, int i) throws IOException, NumberFormatException, IndexOutOfBoundsException, IllegalStateException {
		writer.write("\tvoid draw_layer_" + i + name.replaceAll("\\W", "_") + "() {\n");
		writer.write("\t\tglPushMatrix();\n");
		for (String action : actions.split("\n")) {
			action = action.trim();
			if (action.isEmpty()) {
				continue;
			}
			switch (action.charAt(0)) {
				case '+': {
					Pattern p = Pattern.compile("\\[([^\\]]*)\\]");
					Matcher m = p.matcher(action);
					m.find();
					String pair = m.group(1);
					String first = pair.split(",")[0].trim();
					String second = pair.split(",")[1].trim();
					if (first.startsWith("<")) {
						first = "f" + first.substring(1, first.length() - 1).replaceAll("\\W", "_");
					}
					if (second.startsWith("<")) {
						second = "f" + second.substring(1, second.length() - 1).replaceAll("\\W", "_");
					}
					writer.write("\t\tglTranslatef(" + first + "," + second + ",0);\n");
				}
				break;
				case 'x': {
					Pattern p = Pattern.compile("\\[([^\\]]*)\\]");
					Matcher m = p.matcher(action);
					m.find();
					String pair = m.group(1);
					String first = pair.split(",")[0].trim();
					if (first.startsWith("<")) {
						first = "f" + first.substring(1, first.length() - 2).replaceAll("\\W", "_");
					}
					writer.write("\t\tglRotatef(" + first + ",0,0,1);\n");
				}
				break;
				case '.': {
					Pattern p = Pattern.compile("\\[([^\\]]*)\\]");
					Matcher m = p.matcher(action);
					m.find();
					String pair = m.group(1);
					String first = pair.split(",")[0].trim();
					String second = pair.split(",")[1].trim();
					if (first.startsWith("<")) {
						first = "f" + first.substring(1, first.length() - 2).replaceAll("\\W", "_");
					}
					if (second.startsWith("<")) {
						second = "f" + second.substring(1, second.length() - 2).replaceAll("\\W", "_");
					}
					writer.write("\t\tglScalef(" + first + "," + second + ",0);\n");
				}
				break;
				default:
					break;
			}
		}
		if (dx != 0 || dy != 0) {
			writer.write("\t\tglTranslatef(" + dx + ", " + dy + ", 0);");
		}
		if (scissorMaxX != scissorMinX && scissorMaxY != scissorMinY) {
			writer.write("\t\tglEnable(GL_SCISSOR_TEST);");
			writer.write("\t\tglScissor(" + (scissorMinX - basePoint.x) + ", " + (scissorMinY - basePoint.y) + ", " + (scissorMaxX - scissorMinX) + ", " + (scissorMaxY - scissorMinY) + ");");
		}
		for (Element element : elements) {
			element.export(writer);
		}
		writer.write("\t\tglPopMatrix();\n");
		if (scissorMaxX != scissorMinX && scissorMaxY != scissorMinY) {
			writer.write("\t\tglDisable(GL_SCISSOR_TEST);");
		}
		writer.write("\t} // end draw_layer_" + i + name.replaceAll("\\W", "_") + "()\n\n");
	}

	@SuppressWarnings("AssignmentToForLoopParameter")
	public void drawGL() {
		GL11.glPushMatrix();
		for (String action : actions.split("\n")) {
			action = action.trim();
			if (action.isEmpty()) {
				continue;
			}
			switch (action.charAt(0)) {
				case '+': {
					Pattern p = Pattern.compile("\\[([^\\]]*)\\]");
					Matcher m = p.matcher(action);
					m.find();
					String pair = m.group(1);
					String first = pair.split(",")[0].trim();
					String second = pair.split(",")[1].trim();
					if (first.startsWith("<")) {
						first = "f" + first.substring(1, first.length() - 1).replaceAll("\\W", "_");
					}
					if (second.startsWith("<")) {
						second = "f" + second.substring(1, second.length() - 1).replaceAll("\\W", "_");
					}
					GL11.glTranslatef(GLPanel.Sliders.getValue(first), GLPanel.Sliders.getValue(second), 0);
				}
				break;
				case 'x': {
					Pattern p = Pattern.compile("\\[([^\\]]*)\\]");
					Matcher m = p.matcher(action);
					m.find();
					String pair = m.group(1);
					String first = pair.split(",")[0].trim();
					if (first.startsWith("<")) {
						first = "f" + first.substring(1, first.length() - 2).replaceAll("\\W", "_");
					}
					GL11.glRotatef(GLPanel.Sliders.getValue(first), 0, 0, 1);
				}
				break;
				case '.': {
					Pattern p = Pattern.compile("\\[([^\\]]*)\\]");
					Matcher m = p.matcher(action);
					m.find();
					String pair = m.group(1);
					String first = pair.split(",")[0].trim();
					String second = pair.split(",")[1].trim();
					if (first.startsWith("<")) {
						first = "f" + first.substring(1, first.length() - 2).replaceAll("\\W", "_");
					}
					if (second.startsWith("<")) {
						second = "f" + second.substring(1, second.length() - 2).replaceAll("\\W", "_");
					}
					GL11.glScalef(GLPanel.Sliders.getValue(first), GLPanel.Sliders.getValue(second), 0);
				}
				break;
				default:
					break;
			}
		}
		if (dx != 0 || dy != 0) {
			GL11.glTranslatef(dx, dy, 0);
		}
		if (scissorMaxX != scissorMinX && scissorMaxY != scissorMinY) {
			GL11.glEnable(GL11.GL_SCISSOR_TEST);
			GL11.glScissor(scissorMinX - basePoint.x, scissorMinY - basePoint.y, scissorMaxX - scissorMinX, scissorMaxY - scissorMinY);
		}
		elements.stream().forEach((element) -> element.drawGL());
		GL11.glPopMatrix();
		if (scissorMaxX != scissorMinX && scissorMaxY != scissorMinY) {
			GL11.glDisable(GL11.GL_SCISSOR_TEST);
		}
		// TODO
	}

	public void setScissor(int minX, int minY, int maxX, int maxY) {
		scissorMaxX = maxX;
		scissorMaxY = maxY;
		scissorMinX = minX;
		scissorMinY = minY;
	}

	public int getScissorMinY() {
		return scissorMinY;
	}

	public int getScissorMinX() {
		return scissorMinX;
	}

	public int getScissorMaxY() {
		return scissorMaxY;
	}

	public int getScissorMaxX() {
		return scissorMaxX;
	}

	/**
	 * The actions are the as follows:
	 *
	 * (Every line is a different action and they are executed in order, from
	 * top to bottom.)
	 *
	 * (Every dataref from XPlane is obtained from The Big List of DataRefs:
	 * http://www.xsquawkbox.net/xpsdk/docs/DataRefs.html)
	 *
	 * (Every dataref is entered in between < and >, such as:
	 * <sim/cockpit/gyros/the_ind_deg3>)
	 *
	 * There are three possible operations:
	 *
	 * - Translating the elements: +
	 *
	 * - Rotating the elements: x
	 *
	 * - Scaling the elements: .
	 *
	 * All operations are unary, and operate over a pair of values [x,y], where
	 * x and y can be float values (use dot(.) as separator) or one <dataref>.
	 * It is *not* possible to use any operations other than those 3 unary ones,
	 * to do something complex, just do more operations. Examples:
	 *
	 * + [150,350.480] (translates on the z = 0 plane)
	 *
	 * x [<sim/cockpit/gyros/the_ind_deg3>, 0] (rotates on the z axis, second
	 * parameter should always be 0, but won't be processed)
	 *
	 * . [-1, 2.3] (scales the y axis by 2.3 and flips the x axis)
	 */
}
