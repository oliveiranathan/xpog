/*
 * Copyright (C) 2014, Nathan Oliveira <oliveiranathan(at)gmail(dot)com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package Geom;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.io.BufferedWriter;
import java.io.IOException;
import org.lwjgl.opengl.GL11;

/**
 *
 * @author Nathan Oliveira
 */
public class Circle extends Element {

	private static final long serialVersionUID = 1L;

	private Point center;
	private int radius;

	public Circle(String name, Color color, Point center, int radius) {
		super(name, color);
		this.center = center;
		this.radius = radius;
	}

	@Override
	public void draw(Graphics g) {
		super.draw(g);
		if (isFilled()) {
			g.fillOval(center.x - radius, center.y - radius, 2 * radius, 2 * radius);
		} else {
			g.drawOval(center.x - radius, center.y - radius, 2 * radius, 2 * radius);
		}
	}

	@Override
	public void drawBorder(Graphics g) {
		super.drawBorder(g);
		g.drawOval(center.x - radius, center.y - radius, 2 * radius, 2 * radius);
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public void setCenter(Point center) {
		this.center = center;
	}

	public int getRadius() {
		return radius;
	}

	public Point getCenter() {
		return center;
	}

	@Override
	public void export(BufferedWriter writer) throws IOException {
		super.export(writer);
		Layer parent = getParent();
		if (isFilled()) {
			writer.write("\t\tglBegin(GL_POLYGON);\n");
		} else {
			writer.write("\t\tglBegin(GL_LINE_LOOP);\n");
		}
		double nEdges = 2.0 * Math.PI * radius / 10.0;
		for (double i = 0; i < 2 * Math.PI; i += Math.PI * 2 / nEdges) {
			writer.write("\t\t\tglVertex2f(" + (center.x - parent.getBasePoint().x + Math.cos(i) * radius) + "," + (center.y - parent.getBasePoint().y + Math.sin(i) * radius) + ");\n");
		}
		writer.write("\t\tglEnd();\n");
	}

	@Override
	public void drawGL() {
		super.drawGL();
		Layer parent = getParent();
		if (isFilled()) {
			GL11.glBegin(GL11.GL_POLYGON);
		} else {
			GL11.glBegin(GL11.GL_LINE_LOOP);
		}
		double nEdges = 2.0 * Math.PI * radius / 10.0;
		for (double i = 0; i < 2 * Math.PI; i += Math.PI * 2 / nEdges) {
			GL11.glVertex2f((float) (center.x - parent.getBasePoint().x + Math.cos(i) * radius), (float) (center.y - parent.getBasePoint().y + Math.sin(i) * radius));
		}
		GL11.glEnd();
	}
}
