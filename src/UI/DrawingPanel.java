/*
 * Copyright (C) 2014, Nathan Oliveira <oliveiranathan(at)gmail(dot)com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package UI;

import Geom.Arc;
import Geom.Circle;
import Geom.Element;
import Geom.Instrument;
import Geom.Layer;
import Geom.Line;
import Geom.Rectangle;
import Geom.Triangle;
import Geom.XPanel;
import Geom.XPanelObserver;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JPanel;

/**
 *
 * @author Nathan Oliveira
 */
public class DrawingPanel extends JPanel implements XPanelObserver {

	private static final long serialVersionUID = 1L;

	private DrawingState state = DrawingState.NONE;
	private Point start, middle, now;

	private String instrument = null;
	private String layer = null;
	private Color color = Color.BLACK;
	private Element selectedElement = null;

	public DrawingPanel() {
		setBackground(Color.WHITE);
		MouseAdapter ma = new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				if (state == DrawingState.BASE_POINT) {
					XPanel.getInstance().getInstruments()
							.stream()
							.filter((instr) -> instr.getName().equals(instrument))
							.forEach((instr)
									-> instr.getLayers()
									.stream()
									.filter((layr) -> layr.getName().equals(layer))
									.forEach((layr)
											-> layr.setBasePoint(e.getPoint())));
					repaint();
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				switch (state) {
					case SCISSOR_AREA:
					case LINE:
						start = e.getPoint();
						now = e.getPoint();
						break;
					case QUAD:
						if (start == null) {
							start = e.getPoint();
							now = e.getPoint();
							middle = null;
						}
						break;
					case CIRCLE:
						start = e.getPoint();
						now = e.getPoint();
						break;
					case ARC:
						if (start == null) {
							start = e.getPoint();
							now = e.getPoint();
							middle = null;
						}
						break;
					case TRIANGLE:
						if (start == null) {
							start = e.getPoint();
							now = e.getPoint();
							middle = null;
						}
						break;
				}
				repaint();
			}

			@Override
			public void mouseDragged(MouseEvent e) {
				switch (state) {
					case SCISSOR_AREA:
					case LINE:
						now = e.getPoint();
						break;
					case QUAD:
						if (middle == null) {
							now = e.getPoint();
						}
						break;
					case CIRCLE:
						now = e.getPoint();
						break;
					case ARC:
						if (middle == null) {
							now = e.getPoint();
						}
						break;
					case TRIANGLE:
						if (middle == null) {
							now = e.getPoint();
						}
						break;
				}
				repaint();
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				switch (state) {
					case QUAD:
						if (middle != null) {
							now = e.getPoint();
						}
						break;
					case ARC:
						if (middle != null) {
							now = e.getPoint();
						}
						break;
					case TRIANGLE:
						if (middle != null) {
							now = e.getPoint();
						}
						break;
				}
				repaint();
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				now = e.getPoint();
				if (XPanel.getInstance() != null) {
					XPanel.getInstance().getInstruments()
							.stream()
							.filter((instr) -> instr.getName().equals(instrument))
							.forEach((instr)
									-> instr.getLayers()
									.stream()
									.filter((layr) -> layr.getName().equals(layer))
									.forEach((layr) -> {
										int last = -1;
										for (Element elem : layr.getElements()) {
											if (elem.getName().startsWith("Elemento ")) {
												try {
													last = Integer.parseInt(elem.getName().substring("Elemento ".length()));
												} catch (NumberFormatException ex) {
												}
											}
										}
										if (last != -1) {
											last++;
										} else {
											last = 1;
										}
										String name = "Elemento " + last;
										MainFrame.setNewValue(name);
										switch (state) {
											case SCISSOR_AREA:
												layr.setScissor(start.x < now.x ? start.x : now.x, start.y < now.y ? start.y : now.y, start.x > now.x ? start.x : now.x, start.y > now.y ? start.y : now.y);
												start = null;
												now = null;
												break;
											case LINE:
												layr.addElement(new Line(name, color, start, now));
												start = null;
												now = null;
												break;
											case QUAD:
												if (middle == null) {
													now = e.getPoint();
													middle = e.getPoint();
												} else {
													now = e.getPoint();
													Point[] ps = getQuadPoints();
													layr.addElement(new Rectangle(name, color, start, middle, ps[0], ps[1]));
													start = null;
													middle = null;
													now = null;
												}
												break;
											case CIRCLE:
												layr.addElement(new Circle(name, color, start, (int) start.distance(now)));
												start = null;
												now = null;
												break;
											case ARC:
												if (middle == null) {
													now = e.getPoint();
													middle = e.getPoint();
												} else {
													now = e.getPoint();
													int[] angs = getArcAngles();
													layr.addElement(new Arc(name, color, start, (int) start.distance(middle), angs[0], angs[1]));
													start = null;
													middle = null;
													now = null;
												}
												break;
											case TRIANGLE:
												if (middle == null) {
													now = e.getPoint();
													middle = e.getPoint();
												} else {
													now = e.getPoint();
													layr.addElement(new Triangle(name, color, start, middle, now));
													start = null;
													middle = null;
													now = null;
												}
												break;
										}
									}));
				}
				repaint();
			}
		};
		addMouseListener(ma);
		addMouseMotionListener(ma);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		try {
			if (XPanel.getInstance() == null || instrument == null || layer == null) {
				return;
			}
		} catch (IllegalStateException ex) {
			return;
		}
		XPanel.getInstance().getInstruments()
				.stream()
				.filter((instr) -> (instr.getName().equals(instrument)))
				.forEach((instr) -> {
					g.drawImage(instr.getBackground(), 0, 0, null);
					instr.getLayers()
					.stream()
					.filter((layr) -> (layr.getName().equals(layer)))
					.forEach((layr) -> {
						if (layr.getBasePoint() != null) {
							g.setColor(Color.red);
							g.fillOval(layr.getBasePoint().x - 2, layr.getBasePoint().y - 2, 5, 5);
							g.drawRect(layr.getBasePoint().x - layr.getDx(), layr.getBasePoint().y - layr.getDy(), instr.getW(), instr.getH());
						}
						if (layr.getScissorMaxX() != layr.getScissorMinX() && layr.getScissorMaxY() != layr.getScissorMinY()) {
							g.setColor(Color.red);
							g.drawLine(layr.getScissorMinX(), layr.getScissorMinY(), layr.getScissorMinX(), layr.getScissorMaxY());
							g.drawLine(layr.getScissorMinX(), layr.getScissorMaxY(), layr.getScissorMaxX(), layr.getScissorMaxY());
							g.drawLine(layr.getScissorMaxX(), layr.getScissorMaxY(), layr.getScissorMaxX(), layr.getScissorMinY());
							g.drawLine(layr.getScissorMaxX(), layr.getScissorMinY(), layr.getScissorMinX(), layr.getScissorMinY());
						}
						layr.getElements()
						.stream()
						.forEach((elem)
								-> elem.draw(g));
					});
				});
		if (selectedElement != null) {
			selectedElement.drawBorder(g);
		}
		g.setColor(Color.MAGENTA);
		if (start != null) {
			g.fillOval(start.x - 2, start.y - 2, 5, 5);
		}
		if (middle != null) {
			g.fillOval(middle.x - 2, middle.y - 2, 5, 5);
		}
		g.setColor(color);
		switch (state) {
			case SCISSOR_AREA:
				if (start != null && now != null) {
					int minX = start.x < now.x ? start.x : now.x;
					int minY = start.y < now.y ? start.y : now.y;
					int maxX = start.x > now.x ? start.x : now.x;
					int maxY = start.y > now.y ? start.y : now.y;
					g.drawLine(minX, minY, minX, maxY);
					g.drawLine(minX, maxY, maxX, maxY);
					g.drawLine(maxX, maxY, maxX, minY);
					g.drawLine(maxX, minY, minX, minY);
				}
				break;
			case LINE:
				if (start != null && now != null) {
					g.drawLine(start.x, start.y, now.x, now.y);
				}
				break;
			case QUAD:
				if (start != null && now != null) {
					if (middle == null) {
						g.drawLine(start.x, start.y, now.x, now.y);
					} else {
						Point[] ps = getQuadPoints();
						g.drawLine(start.x, start.y, middle.x, middle.y);
						g.drawLine(middle.x, middle.y, ps[0].x, ps[0].y);
						g.drawLine(ps[0].x, ps[0].y, ps[1].x, ps[1].y);
						g.drawLine(ps[1].x, ps[1].y, start.x, start.y);
					}
				}
				break;
			case CIRCLE:
				if (start != null && now != null) {
					int xc = start.x;
					int yc = start.y;
					int r = (int) start.distance(now);
					g.drawOval(xc - r, yc - r, 2 * r, 2 * r);
				}
				break;
			case ARC:
				if (start != null && now != null) {
					if (middle == null) {
						g.drawLine(start.x, start.y, now.x, now.y);
					} else {
						int[] angs = getArcAngles();
						int r = (int) start.distance(middle);
						g.drawArc(start.x - r, start.y - r, 2 * r, 2 * r, angs[0], angs[1]);
					}
				}
				break;
			case TRIANGLE:
				if (start != null && now != null) {
					if (middle == null) {
						g.drawLine(start.x, start.y, now.x, now.y);
					} else {
						g.drawLine(start.x, start.y, middle.x, middle.y);
						g.drawLine(middle.x, middle.y, now.x, now.y);
						g.drawLine(now.x, now.y, start.x, start.y);
					}
				}
				break;
		}
	}

	private Point[] getQuadPoints() {
		if (start == null || middle == null || now == null) {
			return null;
		}
		Point[] ps = new Point[2];
		double d = middle.distance(now);
		int dx = middle.x - start.x;
		int dy = middle.y - start.y;
		double m = Math.sqrt(dx * dx + dy * dy);
		dx = (int) (dx * d / m);
		dy = (int) (dy * d / m);
		ps[0] = new Point(middle);
		ps[1] = new Point(start);
		ps[0].x += dy;
		ps[0].y -= dx;
		ps[1].x += dy;
		ps[1].y -= dx;
		return ps;
	}

	private int[] getArcAngles() {
		if (start == null || middle == null || now == null) {
			return null;
		}
		int[] angs = new int[2];
		angs[0] = (int) Math.toDegrees(Math.atan2(-middle.y + start.y, middle.x - start.x));
		angs[1] = (int) Math.toDegrees(Math.atan2(-now.y + start.y, now.x - start.x));
		angs[1] -= angs[0];
		if (angs[1] < 0) {
			angs[1] += 360;
		}
		return angs;
	}

	public void resetData() {
		instrument = null;
		layer = null;
		if (XPanel.getInstance() != null) {
			List<Instrument> is = XPanel.getInstance().getInstruments();
			if (!is.isEmpty()) {
				instrument = is.get(0).getName();
				List<Layer> ls = is.get(0).getLayers();
				if (!ls.isEmpty()) {
					layer = ls.get(0).getName();
				}
			}
		}
		repaint();
	}

	public void setLayer(String layer) throws IllegalStateException {
		if (XPanel.getInstance() == null) {
			throw new IllegalStateException("Set the data first.");
		}
		this.layer = layer;
		repaint();
	}

	public void setInstrument(String instrument) throws IllegalStateException {
		if (XPanel.getInstance() == null) {
			throw new IllegalStateException("Set the data first.");
		}
		if (!instrument.equals(this.instrument)) {
			this.instrument = instrument;
			this.layer = null;
		}
		repaint();
	}

	public void setState(DrawingState state) {
		start = null;
		middle = null;
		this.state = state;
	}

	public DrawingState getState() {
		return state;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Element getSelectedElement() {
		return selectedElement;
	}

	public void setSelectedElement(Element element) {
		this.selectedElement = element;
	}

	@Override
	public void notifyObservers() {
		repaint();
	}
}
