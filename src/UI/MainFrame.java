/*
 * Copyright (C) 2014, Nathan Oliveira <oliveiranathan(at)gmail(dot)com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package UI;

import GLRunner.GLPanel;
import Geom.Element;
import Geom.Instrument;
import Geom.Layer;
import Geom.XPanel;
import Geom.XPanelObserver;
import java.awt.AWTException;
import java.awt.Color;
import java.awt.Robot;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Nathan Oliveira
 */
public class MainFrame extends javax.swing.JFrame implements XPanelObserver {

	private static final long serialVersionUID = 1L;

	private DefaultListModel<String> listModelInstruments;
	private DefaultListModel<String> listModelLayers;
	private DefaultListModel<String> listModelElements;
	private static String newValue = null;
	private ButtonGroup btnGrpState;

	// TODO add the OpenGL test panel.
	// TODO add the option to fiddle with the values for testing in OpenGL...
	/**
	 * Creates new form MainFrame
	 */
	public MainFrame() {
		initComponents();
		prepareUI();
	}

	public static void setNewValue(String str) {
		newValue = str;
	}

	@Override
	public void notifyObservers() {
		String instr = listInstruments.getSelectedValue();
		String layer = listLayers.getSelectedValue();
		String elemr = listElements.getSelectedValue();
		disableAllBtns();
		updateInstruments();
		updateLayers();
		updateElements();
		if (!listModelInstruments.contains(instr) && listInstruments.getSelectedIndex() == -1) {
			listInstruments.setSelectedValue(newValue, true);
		} else {
			listInstruments.setSelectedValue(instr, true);
		}
		if (!listModelLayers.contains(layer) && listLayers.getSelectedIndex() == -1) {
			listLayers.setSelectedValue(newValue, true);
		} else {
			listLayers.setSelectedValue(layer, true);
		}
		if (!listModelInstruments.contains(elemr) && listInstruments.getSelectedIndex() == -1) {
			listElements.setSelectedValue(newValue, true);
		} else {
			listElements.setSelectedValue(elemr, true);
		}
		newValue = null;
	}

	// <editor-fold defaultstate="collapsed" desc="Listeners">
	private void prepareUI() {
		labelColor.setBackground(new Color((Integer) spnRed.getValue(), (Integer) spnGreen.getValue(), (Integer) spnBlue.getValue()));
		menuQuit.addActionListener((e) -> dispose());
		menuNew.addActionListener((e) -> {
			XPanel.createInstance();
			((DrawingPanel) panelDrawing).resetData();
			XPanel.addObserver(this);
			XPanel.addObserver((XPanelObserver) panelDrawing);
			btnAddInstrument.setEnabled(true);
			disableAllBtns();
			notifyObservers();
		});
		menuOpen.addActionListener((e) -> {
			JFileChooser fc = new JFileChooser();
			fc.setFileFilter(new FileNameExtensionFilter("XPlane cockpit save", "xppg"));
			if (fc.showOpenDialog(null) != JFileChooser.APPROVE_OPTION) {
				return;
			}
			try {
				XPanel.createInstance(fc.getSelectedFile());
				((DrawingPanel) panelDrawing).resetData();
				XPanel.addObserver(this);
				XPanel.addObserver((XPanelObserver) panelDrawing);
				btnAddInstrument.setEnabled(true);
				notifyObservers();
				listInstruments.setSelectedIndex(0);
				listLayers.setSelectedIndex(0);
			} catch (ClassNotFoundException | IOException ex) {
				statusLabel.setText("Erro ao carregar o arquivo.");
			}
		});
		menuSave.addActionListener((e) -> {
			if (XPanel.getInstance().getFile() == null) {
				JFileChooser fc = new JFileChooser();
				fc.setFileFilter(new FileNameExtensionFilter("XPlane cockpit save", "xppg"));
				if (fc.showSaveDialog(null) != JFileChooser.APPROVE_OPTION) {
					return;
				}
				File f = fc.getSelectedFile();
				if (!f.getAbsolutePath().endsWith(".xppg")) {
					f = new File(f.getAbsolutePath() + ".xppg");
				}
				XPanel.getInstance().setFile(f);
			}
			try {
				XPanel.getInstance().saveFile();
			} catch (IOException ex) {
				statusLabel.setText("Erro ao salvar o arquivo.");
			}
		});
		menuExport.addActionListener((e) -> {
			JFileChooser fc = new JFileChooser();
			fc.setFileFilter(new FileNameExtensionFilter("C++ file", "cpp"));
			if (fc.showSaveDialog(null) != JFileChooser.APPROVE_OPTION) {
				return;
			}
			File f = fc.getSelectedFile();
			if (!f.getAbsolutePath().endsWith(".cpp")) {
				f = new File(f.getAbsolutePath() + ".cpp");
			}
			try {
				XPanel.getInstance().export(f);
			} catch (IOException ex) {
				Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
				JOptionPane.showMessageDialog(null, "Erro ao exportar para o arquivo.", "Erro", JOptionPane.ERROR_MESSAGE);
			} catch (IllegalStateException | IndexOutOfBoundsException | NumberFormatException | ParseException ex) {
				Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
				JOptionPane.showMessageDialog(null, "Erro ao processar as ações.", "Erro", JOptionPane.ERROR_MESSAGE);
			}
		});
		listInstruments.addListSelectionListener((e) -> {
			disableAllBtns();
			listModelLayers.removeAllElements();
			listLayers.clearSelection();
			listElements.clearSelection();
			if (e.getValueIsAdjusting() || listInstruments.getSelectedIndex() == -1) {
				return;
			}
			btnAddLayer.setEnabled(true);
			XPanel.getInstance().getInstruments()
					.stream()
					.filter((instrument) -> instrument.getName().equals(listInstruments.getSelectedValue()))
					.forEach((instrument) -> {
						((DrawingPanel) panelDrawing).setInstrument(instrument.getName());
						instrument.getLayers()
						.stream()
						.forEach((layer) -> listModelLayers.addElement(layer.getName()));
					});
		});
		listLayers.addListSelectionListener((e) -> {
			disableAllBtns();
			btnAddLayer.setEnabled(true);
			listElements.clearSelection();
			if (e.getValueIsAdjusting() || listLayers.getSelectedIndex() == -1) {
				return;
			}
			enableAllBtns();
			XPanel.getInstance().getInstruments()
					.stream()
					.filter((instrument) -> instrument.getName().equals(listInstruments.getSelectedValue()))
					.forEach((instrument)
							-> instrument.getLayers()
							.stream()
							.filter((layer) -> layer.getName().equals(listLayers.getSelectedValue()))
							.forEach((layer) -> {
								((DrawingPanel) panelDrawing).setLayer(layer.getName());
								listModelElements.removeAllElements();
								listElements.clearSelection();
								layer.getElements()
								.stream()
								.forEach((element) -> listModelElements.addElement(element.getName()));
							}));
		});
		btnAddInstrument.addActionListener((e) -> {
			int last = -1;
			Object[] instruments = listModelInstruments.toArray();
			for (Object instrument : instruments) {
				if (((String) instrument).startsWith("Instrumento ")) {
					try {
						last = Integer.parseInt(((String) instrument).substring("Instrumento ".length()));
					} catch (NumberFormatException ex) {
					}
				}
			}
			if (last != -1) {
				last++;
			} else {
				last = 1;
			}
			String newInstrument;
			boolean nameIsUnique;
			do {
				newInstrument = JOptionPane.showInputDialog("Entre com o nome do instrumento:", "Instrumento " + last);
				nameIsUnique = true;
				for (Object instrument : instruments) {
					if (instrument.equals(newInstrument)) {
						nameIsUnique = false;
						break;
					}
				}
			} while (!nameIsUnique || newInstrument == null || newInstrument.isEmpty());
			XPanel.getInstance().addInstrument(newInstrument);
			updateInstruments();
			listInstruments.setSelectedValue(newInstrument, true);
			((DrawingPanel) panelDrawing).setInstrument(newInstrument);
			updateLayers();
		});
		btnAddLayer.addActionListener((e) -> {
			int last = -1;
			Object[] layers = listModelLayers.toArray();
			for (Object layer : layers) {
				if (((String) layer).startsWith("Camada ")) {
					try {
						last = Integer.parseInt(((String) layer).substring("Camada ".length()));
					} catch (NumberFormatException ex) {
					}
				}
			}
			if (last != -1) {
				last++;
			} else {
				last = 1;
			}
			String newLayer;
			boolean nameIsUnique;
			do {
				newLayer = JOptionPane.showInputDialog("Entre com o nome da camada:", "Camada " + last);
				nameIsUnique = true;
				for (Object layer : layers) {
					if (layer.equals(newLayer)) {
						nameIsUnique = false;
						break;
					}
				}
			} while (!nameIsUnique || newLayer == null || newLayer.isEmpty());
			String finalNewLayer = newLayer; // GAMBI
			XPanel.getInstance().getInstruments()
					.stream()
					.filter((instrument) -> instrument.getName().equals(listInstruments.getSelectedValue()))
					.forEach((instrument) -> instrument.addLayer(finalNewLayer));
			updateLayers();
			listLayers.setSelectedValue(finalNewLayer, true);
			((DrawingPanel) panelDrawing).setLayer(finalNewLayer);
			updateElements();
		});
		listElements.addListSelectionListener((e) -> {
			((DrawingPanel) panelDrawing).setSelectedElement(null);
			XPanel.getInstance().getInstruments()
					.stream()
					.filter((instr) -> instr.getName().equals(listInstruments.getSelectedValue()))
					.forEach((instr) -> {
						instr.getLayers()
						.stream()
						.filter((layr) -> layr.getName().equals(listLayers.getSelectedValue()))
						.forEach((layr) -> {
							layr.getElements()
							.stream()
							.filter((elem) -> elem.getName().equals(listElements.getSelectedValue()))
							.forEach((elem) -> {
								((DrawingPanel) panelDrawing).setSelectedElement(elem);
							});
						});
					});
			panelDrawing.repaint();
		});
		btnGrpState = new ButtonGroup();
		btnGrpState.add(btnArc);
		btnGrpState.add(btnBasePoint);
		btnGrpState.add(btnCircle);
		btnGrpState.add(btnLine);
		btnGrpState.add(btnRect);
		btnGrpState.add(btnTriang);
		btnGrpState.add(btnScissorArea);
		prepareTools();
		preparePopups();
	}

	private void preparePopups() {
		listInstruments.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showInstrumentsPopup(e);
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showInstrumentsPopup(e);
				}
			}

			private void showInstrumentsPopup(MouseEvent evt) {
				Instrument instrument = null;
				for (Instrument instr : XPanel.getInstance().getInstruments()) {
					if (instr.getName().equals(listInstruments.getSelectedValue())) {
						instrument = instr;
					}
				}
				if (instrument == null) {
					return;
				}
				Instrument finalInstrument = instrument;
				JPopupMenu popup = new JPopupMenu();
				popup.add(new JLabel("Nome"));
				JTextField tf = new JTextField(instrument.getName(), 15);
				popup.add(tf);
				tf.addActionListener((e) -> {
					if (!tf.getText().equals(listInstruments.getSelectedValue())) {
						newValue = tf.getText();
						finalInstrument.setName(newValue);
					}
				});
				popup.add(new JLabel("Posição e tamanho:"));
				popup.add(new JLabel("X:"));
				JTextField xf = new JTextField("" + finalInstrument.getX(), 15);
				popup.add(xf);
				xf.addActionListener((e) -> {
					try {
						finalInstrument.setX(Integer.parseInt(xf.getText()));
					} catch (NumberFormatException ex) {
					}
				});
				popup.add(new JLabel("Y:"));
				JTextField yf = new JTextField("" + finalInstrument.getY(), 15);
				popup.add(yf);
				yf.addActionListener((e) -> {
					try {
						finalInstrument.setY(Integer.parseInt(yf.getText()));
					} catch (NumberFormatException ex) {
					}
				});
				popup.add(new JLabel("W:"));
				JTextField wf = new JTextField("" + finalInstrument.getW(), 15);
				popup.add(wf);
				wf.addActionListener((e) -> {
					try {
						finalInstrument.setW(Integer.parseInt(wf.getText()));
					} catch (NumberFormatException ex) {
					}
				});
				popup.add(new JLabel("H:"));
				JTextField hf = new JTextField("" + finalInstrument.getH(), 15);
				popup.add(hf);
				hf.addActionListener((e) -> {
					try {
						finalInstrument.setH(Integer.parseInt(hf.getText()));
					} catch (NumberFormatException ex) {
					}
				});
				popup.addSeparator();
				JMenuItem item = new JMenuItem("Subir um item");
				item.addActionListener((e) -> XPanel.getInstance().swapInstruments(listInstruments.getSelectedIndex(), listInstruments.getSelectedIndex() - 1));
				popup.add(item);
				item = new JMenuItem("Descer um item");
				item.addActionListener((e) -> XPanel.getInstance().swapInstruments(listInstruments.getSelectedIndex(), listInstruments.getSelectedIndex() + 1));
				popup.add(item);
				item = new JMenuItem("Mover para o topo");
				item.addActionListener((e) -> XPanel.getInstance().swapInstruments(listInstruments.getSelectedIndex(), 0));
				popup.add(item);
				item = new JMenuItem("Mover para a base");
				item.addActionListener((e) -> XPanel.getInstance().swapInstruments(listInstruments.getSelectedIndex(), listModelInstruments.getSize() - 1));
				popup.add(item);
				popup.addSeparator();
				item = new JMenuItem("Deletar");
				item.addActionListener((e) -> XPanel.getInstance().removeInstrument(finalInstrument));
				popup.add(item);
				popup.show(listInstruments, evt.getX(), evt.getY());
			}
		});
		listLayers.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showLayersPopup(e);
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showLayersPopup(e);
				}
			}

			private void showLayersPopup(MouseEvent evt) {
				Instrument instrument = null;
				for (Instrument instr : XPanel.getInstance().getInstruments()) {
					if (instr.getName().equals(listInstruments.getSelectedValue())) {
						instrument = instr;
					}
				}
				if (instrument == null) {
					return;
				}
				Instrument finalInstrument = instrument;
				Layer layer = null;
				for (Layer layr : finalInstrument.getLayers()) {
					if (layr.getName().equals(listLayers.getSelectedValue())) {
						layer = layr;
					}
				}
				if (layer == null) {
					return;
				}
				Layer finalLayer = layer;
				JPopupMenu popup = new JPopupMenu();
				popup.add(new JLabel("Nome"));
				JTextField tf = new JTextField(finalLayer.getName(), 15);
				popup.add(tf);
				tf.addActionListener((e) -> {
					if (!tf.getText().equals(listLayers.getSelectedValue())) {
						newValue = tf.getText();
						finalLayer.setName(newValue);
					}
				});
				popup.add(new JLabel("Posição(em relação ao início do instrumento):"));
				popup.add(new JLabel("X:"));
				JTextField xf = new JTextField("" + finalLayer.getDx(), 15);
				popup.add(xf);
				xf.addActionListener((e) -> {
					try {
						finalLayer.setDx(Integer.parseInt(xf.getText()));
					} catch (NumberFormatException ex) {
					}
				});
				popup.add(new JLabel("Y:"));
				JTextField yf = new JTextField("" + finalLayer.getDy(), 15);
				popup.add(yf);
				yf.addActionListener((e) -> {
					try {
						finalLayer.setDy(Integer.parseInt(yf.getText()));
					} catch (NumberFormatException ex) {
					}
				});
				popup.addSeparator();
				JMenuItem item = new JMenuItem("Subir um item");
				item.addActionListener((e) -> finalInstrument.swapLayers(listLayers.getSelectedIndex(), listLayers.getSelectedIndex() - 1));
				popup.add(item);
				item = new JMenuItem("Descer um item");
				item.addActionListener((e) -> finalInstrument.swapLayers(listLayers.getSelectedIndex(), listLayers.getSelectedIndex() + 1));
				popup.add(item);
				item = new JMenuItem("Mover para o topo");
				item.addActionListener((e) -> finalInstrument.swapLayers(listLayers.getSelectedIndex(), 0));
				popup.add(item);
				item = new JMenuItem("Mover para a base");
				item.addActionListener((e) -> finalInstrument.swapLayers(listLayers.getSelectedIndex(), listModelLayers.getSize() - 1));
				popup.add(item);
				popup.addSeparator();
				item = new JMenuItem("Deletar");
				item.addActionListener((e) -> finalInstrument.removeLayer(finalLayer));
				popup.add(item);
				popup.addSeparator();
				popup.add(new JLabel("Comportamento da camada:"));
				JTextArea area = new JTextArea(finalLayer.getActions(), 5, 80);
				area.getDocument().addDocumentListener(new DocumentListener() {
					@Override
					public void insertUpdate(DocumentEvent e) {
						update();
					}

					@Override
					public void removeUpdate(DocumentEvent e) {
						update();
					}

					@Override
					public void changedUpdate(DocumentEvent e) {
						update();
					}

					private void update() {
						finalLayer.setActions(area.getText());
					}
				});
				popup.add(new JScrollPane(area));
				popup.show(listLayers, evt.getX(), evt.getY());
			}
		});
		listElements.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showElementsPopup(e);
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showElementsPopup(e);
				}
			}

			private void showElementsPopup(MouseEvent evt) {
				Instrument instrument = null;
				for (Instrument instr : XPanel.getInstance().getInstruments()) {
					if (instr.getName().equals(listInstruments.getSelectedValue())) {
						instrument = instr;
					}
				}
				if (instrument == null) {
					return;
				}
				Instrument finalInstrument = instrument;
				Layer layer = null;
				for (Layer layr : finalInstrument.getLayers()) {
					if (layr.getName().equals(listLayers.getSelectedValue())) {
						layer = layr;
					}
				}
				if (layer == null) {
					return;
				}
				Layer finalLayer = layer;
				Element element = null;
				for (Element elem : layer.getElements()) {
					if (elem.getName().equals(listElements.getSelectedValue())) {
						element = elem;
					}
				}
				if (element == null) {
					return;
				}
				Element finalElement = element;
				JPopupMenu popup = new JPopupMenu();
				popup.add(new JLabel("Nome"));
				JTextField tf = new JTextField(finalElement.getName(), 15);
				popup.add(tf);
				tf.addActionListener((e) -> {
					if (!tf.getText().equals(listElements.getSelectedValue())) {
						newValue = tf.getText();
						finalElement.setName(newValue);
					}
				});
				JCheckBox cb = new JCheckBox("Preenchido?", finalElement.isFilled());
				cb.addActionListener((e) -> {
					finalElement.setFilled(cb.isSelected());
					notifyObservers();
				});
				popup.add(cb);
				popup.addSeparator();
				JMenuItem item = new JMenuItem("Subir um item");
				item.addActionListener((e) -> finalLayer.swapElements(listElements.getSelectedIndex(), listElements.getSelectedIndex() - 1));
				popup.add(item);
				item = new JMenuItem("Descer um item");
				item.addActionListener((e) -> finalLayer.swapElements(listElements.getSelectedIndex(), listElements.getSelectedIndex() + 1));
				popup.add(item);
				item = new JMenuItem("Mover para o topo");
				item.addActionListener((e) -> finalLayer.swapElements(listElements.getSelectedIndex(), 0));
				popup.add(item);
				item = new JMenuItem("Mover para a base");
				item.addActionListener((e) -> finalLayer.swapElements(listElements.getSelectedIndex(), listModelElements.getSize() - 1));
				popup.add(item);
				popup.addSeparator();
				item = new JMenuItem("Deletar");
				item.addActionListener((e) -> finalLayer.removeElement(finalElement));
				popup.add(item);
				popup.show(listElements, evt.getX(), evt.getY());
			}
		});
	}

	private void prepareTools() {
		btnLoadImage.addActionListener((e) -> {
			JFileChooser fc = new JFileChooser();
			if (fc.showOpenDialog(null) != JFileChooser.APPROVE_OPTION) {
				return;
			}
			XPanel.getInstance().getInstruments()
					.stream()
					.filter((instr) -> instr.getName().equals(listInstruments.getSelectedValue()))
					.forEach((instr) -> {
						try {
							instr.setBackground(ImageIO.read(fc.getSelectedFile()));
						} catch (IOException ex) {
							statusLabel.setText("Erro ao carregar a imagem.");
						}
					});
		});
		btnSelColor.addActionListener((e) -> {
			JFrame frame = new JFrame();
			frame.setUndecorated(true);
			frame.setBackground(new Color(255, 255, 255, 1));
			frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
			frame.setVisible(true);
			frame.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {
					try {
						Robot r = new Robot();
						Color c = r.getPixelColor(e.getXOnScreen(), e.getYOnScreen());
						spnRed.setValue(c.getRed());
						spnGreen.setValue(c.getGreen());
						spnBlue.setValue(c.getBlue());
						// TODO update selected element
						// TODO display the selected element...
						frame.dispose();
					} catch (AWTException ex) {
						Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			});
		});
		btnLine.addActionListener((e) -> {
			if (((DrawingPanel) panelDrawing).getState() == DrawingState.LINE) {
				((DrawingPanel) panelDrawing).setState(DrawingState.NONE);
				btnGrpState.clearSelection();
			} else {
				((DrawingPanel) panelDrawing).setState(DrawingState.LINE);
			}
		});
		btnRect.addActionListener((e) -> {
			if (((DrawingPanel) panelDrawing).getState() == DrawingState.QUAD) {
				((DrawingPanel) panelDrawing).setState(DrawingState.NONE);
				btnGrpState.clearSelection();
			} else {
				((DrawingPanel) panelDrawing).setState(DrawingState.QUAD);
			}
		});
		btnArc.addActionListener((e) -> {
			if (((DrawingPanel) panelDrawing).getState() == DrawingState.ARC) {
				((DrawingPanel) panelDrawing).setState(DrawingState.NONE);
				btnGrpState.clearSelection();
			} else {
				((DrawingPanel) panelDrawing).setState(DrawingState.ARC);
			}
		});
		btnCircle.addActionListener((e) -> {
			if (((DrawingPanel) panelDrawing).getState() == DrawingState.CIRCLE) {
				((DrawingPanel) panelDrawing).setState(DrawingState.NONE);
				btnGrpState.clearSelection();
			} else {
				((DrawingPanel) panelDrawing).setState(DrawingState.CIRCLE);
			}
		});
		btnTriang.addActionListener((e) -> {
			if (((DrawingPanel) panelDrawing).getState() == DrawingState.TRIANGLE) {
				((DrawingPanel) panelDrawing).setState(DrawingState.NONE);
				btnGrpState.clearSelection();
			} else {
				((DrawingPanel) panelDrawing).setState(DrawingState.TRIANGLE);
			}
		});
		btnBasePoint.addActionListener((e) -> {
			if (((DrawingPanel) panelDrawing).getState() == DrawingState.BASE_POINT) {
				((DrawingPanel) panelDrawing).setState(DrawingState.NONE);
				btnGrpState.clearSelection();
			} else {
				((DrawingPanel) panelDrawing).setState(DrawingState.BASE_POINT);
			}
		});
		btnScissorArea.addActionListener((e) -> {
			if (((DrawingPanel) panelDrawing).getState() == DrawingState.SCISSOR_AREA) {
				((DrawingPanel) panelDrawing).setState(DrawingState.NONE);
				btnGrpState.clearSelection();
			} else {
				((DrawingPanel) panelDrawing).setState(DrawingState.SCISSOR_AREA);
			}
		});
		ChangeListener l = (ChangeEvent e) -> {
			labelColor.setBackground(new Color((Integer) spnRed.getValue(),
					(Integer) spnGreen.getValue(), (Integer) spnBlue.getValue()));
			((DrawingPanel) panelDrawing).setColor(new Color((Integer) spnRed.getValue(),
					(Integer) spnGreen.getValue(), (Integer) spnBlue.getValue()));
		};
		spnRed.addChangeListener(l);
		spnGreen.addChangeListener(l);
		spnBlue.addChangeListener(l);
		btnOGL.addActionListener((e) -> {
			String selected = listInstruments.getSelectedValue();
			if (selected == null || selected.isEmpty()) {
				return;
			}
			Instrument instrument = null;
			for (Instrument instr : XPanel.getInstance().getInstruments()) {
				if (instr.getName().equals(selected)) {
					instrument = instr;
					break;
				}
			}
			if (instrument == null) {
				return;
			}
			GLPanel glp = new GLPanel(instrument);
			glp.start();
		});
	}

	private void updateElements() {
		String selected = listElements.getSelectedValue();
		listModelElements.removeAllElements();
		listElements.clearSelection();
		XPanel.getInstance().getInstruments()
				.stream()
				.filter((instrument) -> instrument.getName().equals(listInstruments.getSelectedValue()))
				.forEach((instrument)
						-> instrument.getLayers()
						.stream()
						.filter((layer) -> layer.getName().equals(listLayers.getSelectedValue()))
						.forEach((layer)
								-> layer.getElements()
								.stream()
								.forEach((element)
										-> listModelElements.addElement(element.getName()))));
		listElements.setSelectedValue(selected, true);
	}

	private void updateLayers() {
		String selected = listLayers.getSelectedValue();
		listModelLayers.removeAllElements();
		listLayers.clearSelection();
		XPanel.getInstance().getInstruments()
				.stream()
				.filter((instrument) -> instrument.getName().equals(listInstruments.getSelectedValue()))
				.forEach((instrument)
						-> instrument.getLayers()
						.stream()
						.forEach((layer) -> listModelLayers.addElement(layer.getName())));
		listInstruments.setSelectedValue(selected, true);
	}

	private void updateInstruments() {
		String selected = listInstruments.getSelectedValue();
		listModelInstruments.removeAllElements();
		listInstruments.clearSelection();
		XPanel.getInstance().getInstruments()
				.stream()
				.forEach((instrument)
						-> listModelInstruments.addElement(instrument.getName()));
		listInstruments.setSelectedValue(selected, true);
	}

	private void enableAllBtns() {
		btnArc.setEnabled(true);
		btnCircle.setEnabled(true);
		btnLine.setEnabled(true);
		btnLoadImage.setEnabled(true);
		btnRect.setEnabled(true);
		btnBasePoint.setEnabled(true);
		btnTriang.setEnabled(true);
		btnScissorArea.setEnabled(true);
		btnOGL.setEnabled(true);
	}

	private void disableAllBtns() {
		btnAddLayer.setEnabled(false);
		btnArc.setEnabled(false);
		btnCircle.setEnabled(false);
		btnLine.setEnabled(false);
		btnLoadImage.setEnabled(false);
		btnRect.setEnabled(false);
		btnBasePoint.setEnabled(false);
		btnTriang.setEnabled(false);
		btnScissorArea.setEnabled(false);
		btnOGL.setEnabled(false);
	}
	// </editor-fold>

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings({"unchecked", "Convert2Diamond"})
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listInstruments = new javax.swing.JList<String>();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        listLayers = new javax.swing.JList<String>();
        btnAddInstrument = new javax.swing.JButton();
        btnAddLayer = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        statusLabel = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btnLoadImage = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        spnRed = new javax.swing.JSpinner();
        jLabel7 = new javax.swing.JLabel();
        spnGreen = new javax.swing.JSpinner();
        jLabel8 = new javax.swing.JLabel();
        spnBlue = new javax.swing.JSpinner();
        btnSelColor = new javax.swing.JButton();
        labelColor = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        listElements = new javax.swing.JList<String>();
        btnLine = new javax.swing.JToggleButton();
        btnArc = new javax.swing.JToggleButton();
        btnRect = new javax.swing.JToggleButton();
        btnCircle = new javax.swing.JToggleButton();
        btnTriang = new javax.swing.JToggleButton();
        btnBasePoint = new javax.swing.JToggleButton();
        btnScissorArea = new javax.swing.JToggleButton();
        btnOGL = new javax.swing.JButton();
        panelDrawing = new DrawingPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        menuNew = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuOpen = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSave = new javax.swing.JMenuItem();
        menuExport = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuQuit = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("X-Plane cockpit generator");

        jLabel1.setText("Instrumentos");

        listInstruments.setModel(listModelInstruments = new DefaultListModel<>()
        );
        listInstruments.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(listInstruments);

        jLabel2.setText("Camadas");

        listLayers.setModel(listModelLayers = new DefaultListModel<>());
        listLayers.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(listLayers);

        btnAddInstrument.setMnemonic('i');
        btnAddInstrument.setText("Adicionar");
        btnAddInstrument.setEnabled(false);

        btnAddLayer.setMnemonic('c');
        btnAddLayer.setText("Adicionar");
        btnAddLayer.setEnabled(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnAddLayer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnAddInstrument, javax.swing.GroupLayout.DEFAULT_SIZE, 96, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(btnAddInstrument))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addComponent(btnAddLayer))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(statusLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(statusLabel)
                .addContainerGap())
        );

        jLabel3.setText("Elementos");

        jLabel4.setText("Ferramentas");

        btnLoadImage.setMnemonic('b');
        btnLoadImage.setText("Carregar imagem base");
        btnLoadImage.setEnabled(false);

        jLabel5.setText("Cor");

        jLabel6.setText("Vermelho");

        spnRed.setModel(new javax.swing.SpinnerNumberModel(0, 0, 255, 1));

        jLabel7.setText("Verde");

        spnGreen.setModel(new javax.swing.SpinnerNumberModel(0, 0, 255, 1));

        jLabel8.setText("Azul");

        spnBlue.setModel(new javax.swing.SpinnerNumberModel(0, 0, 255, 1));

        btnSelColor.setMnemonic('s');
        btnSelColor.setText("Selecionar cor");

        labelColor.setBackground(new java.awt.Color(0, 0, 0));
        labelColor.setOpaque(true);

        listElements.setModel(listModelElements = new DefaultListModel<>());
        jScrollPane3.setViewportView(listElements);

        btnLine.setText("Linha");
        btnLine.setEnabled(false);

        btnArc.setText("Arco");
        btnArc.setEnabled(false);

        btnRect.setText("Retângulo");
        btnRect.setEnabled(false);

        btnCircle.setText("Círculo");
        btnCircle.setEnabled(false);

        btnTriang.setText("Triângulo");
        btnTriang.setEnabled(false);

        btnBasePoint.setText("Definir ponto base");
        btnBasePoint.setEnabled(false);

        btnScissorArea.setText("Definir area de corte");
        btnScissorArea.setEnabled(false);

        btnOGL.setText("Abrir painel OpenGL");
        btnOGL.setEnabled(false);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSelColor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(47, 47, 47)
                        .addComponent(labelColor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(spnBlue)
                            .addComponent(spnGreen)
                            .addComponent(spnRed)))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(btnBasePoint, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnLoadImage, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel4)
                                .addComponent(jLabel3)
                                .addGroup(jPanel3Layout.createSequentialGroup()
                                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(btnLine, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnArc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(btnRect, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnCircle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(btnTriang, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                            .addComponent(btnScissorArea, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnOGL, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnLine)
                    .addComponent(btnRect))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnArc)
                    .addComponent(btnCircle))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTriang)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnLoadImage)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnBasePoint)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnScissorArea)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnOGL)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(labelColor, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(spnRed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(spnGreen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(spnBlue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSelColor)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout panelDrawingLayout = new javax.swing.GroupLayout(panelDrawing);
        panelDrawing.setLayout(panelDrawingLayout);
        panelDrawingLayout.setHorizontalGroup(
            panelDrawingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 809, Short.MAX_VALUE)
        );
        panelDrawingLayout.setVerticalGroup(
            panelDrawingLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 731, Short.MAX_VALUE)
        );

        jMenu1.setMnemonic('a');
        jMenu1.setText("Arquivo");

        menuNew.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        menuNew.setText("Novo");
        jMenu1.add(menuNew);
        jMenu1.add(jSeparator3);

        menuOpen.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        menuOpen.setText("Abrir");
        jMenu1.add(menuOpen);
        jMenu1.add(jSeparator1);

        menuSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        menuSave.setText("Salvar");
        jMenu1.add(menuSave);

        menuExport.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        menuExport.setText("Exportar");
        jMenu1.add(menuExport);
        jMenu1.add(jSeparator2);

        menuQuit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        menuQuit.setText("Sair");
        jMenu1.add(menuQuit);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelDrawing, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelDrawing, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		/* Set the Nimbus look and feel */
		//<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
		 * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
		//</editor-fold>

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(() -> new MainFrame().setVisible(true));
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddInstrument;
    private javax.swing.JButton btnAddLayer;
    private javax.swing.JToggleButton btnArc;
    private javax.swing.JToggleButton btnBasePoint;
    private javax.swing.JToggleButton btnCircle;
    private javax.swing.JToggleButton btnLine;
    private javax.swing.JButton btnLoadImage;
    private javax.swing.JButton btnOGL;
    private javax.swing.JToggleButton btnRect;
    private javax.swing.JToggleButton btnScissorArea;
    private javax.swing.JButton btnSelColor;
    private javax.swing.JToggleButton btnTriang;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JLabel labelColor;
    private javax.swing.JList<String> listElements;
    private javax.swing.JList<String> listInstruments;
    private javax.swing.JList<String> listLayers;
    private javax.swing.JMenuItem menuExport;
    private javax.swing.JMenuItem menuNew;
    private javax.swing.JMenuItem menuOpen;
    private javax.swing.JMenuItem menuQuit;
    private javax.swing.JMenuItem menuSave;
    private javax.swing.JPanel panelDrawing;
    private javax.swing.JSpinner spnBlue;
    private javax.swing.JSpinner spnGreen;
    private javax.swing.JSpinner spnRed;
    private javax.swing.JLabel statusLabel;
    // End of variables declaration//GEN-END:variables
}
